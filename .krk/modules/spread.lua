local fs = fs

function makeinstall() 
    local file = fs.open(home.."/modules/obfuszip.lua")
    local ziplib = file.readAll()
    file.close()
    file = fs.open(home.."/res/installer.lua")
    local installer = file.readAll()
    file.close()
    local code = [[
        local function loadZipLib()
    ]]..ziplib..[[
        end
        local zip = setmetatable({}, {__index = _G})
        setfenv(loadZipLib, zip)
        loadZipLib()

    ]]..installer

end