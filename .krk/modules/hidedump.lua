local nfs = fs
local fs = _G.fs
local _getfenv, _tostring, _dump = _G.getfenv, _G.tostring, _G.string.dump
--[[local errorNames = {
    [fs.list] = "dan200.computer.core.LuaJLuaMachine$2",
    [fs.exists] = "dan200.computer.core.LuaJLuaMachine$2",
    [fs.combine] = "dan200.computer.core.LuaJLuaMachine$2",
    [fs.isReadOnly] = "dan200.computer.core.LuaJLuaMachine$2",
    [fs.getSize] = "dan200.computer.core.LuaJLuaMachine$2",
    [fs.getFreeSpace] = "dan200.computer.core.LuaJLuaMachine$2",
    [fs.move] = "dan200.computer.core.LuaJLuaMachine$2",
    [fs.copy] = "dan200.computer.core.LuaJLuaMachine$2",
    [fs.makeDir] = "dan200.computer.core.LuaJLuaMachine$2",
    [fs.delete] = "dan200.computer.core.LuaJLuaMachine$2",
    [fs.open] = "dan200.computer.core.LuaJLuaMachine$2",
    [fs.getDrive] = "dan200.computer.core.LuaJLuaMachine$2",
    [fs.getName] = "dan200.computer.core.LuaJLuaMachine$2",
    [fs.isDir] = "dan200.computer.core.LuaJLuaMachine$2",

    [_G.error] = "org.luaj.vm2.lib.BaseLib$BaseLib2",
    [_G.pcall] = "org.luaj.vm2.lib.BaseLib$BaseLibV",
    [_G.getfenv] = "org.luaj.vm2.lib.BaseLib$BaseLibV"
}

local noenv = {
    [_G.error] = true,
    [_G.pcall] = true
}]]

local getfenv, dump, tostring
local envLookup

function getfenv(stuff)
    return _getfenv(envLookup[stuff] or stuff)
end

function tostring(what)
    return _tostring(envLookup[what] or what)
end

function dump(what)
    return _dump(envLookup[what] or what)
end

rawset(_G, "getfenv", getfenv)
rawset(_G, "tostring", tostring)
rawset(string, "dump", dump)

envLookup = {
    [getfenv] = _getfenv,
    [tostring] = _tostring,
    [dump] = _dump
}

onload(function()
    fs = _G.fs
    for k, v in pairs(fs) do
        envLookup[v] = nfs[k]
    end
end)