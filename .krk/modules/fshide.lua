local oldfs = fs
local fs, pairs = fs, pairs

local redirect = {
    ["/startup"] = home.."/startup",
    [home] = home.."/.krk"
}

local function translateName(name) 
    name = fs_combine("/", name)
    if not startwith(name, "/") then name = "/"..name end
    for k, v in pairs(redirect) do
        if startwith(name, k) then
            name = v..(#name == #v and "" or name:sub(#v + 1))
            return name
        end
    end
    return name
end

local newfs = {
    list = function (path_)
        path = translateName(path_)
        local ret = oldfs.list(path)
        local k = 1
        while k <= #ret do
            local v = ret[k]
            local actualpath = translateName(oldfs.combine(path_, v))
            if not oldfs.exists(actualpath) then 
                table.remove(ret, k)
            else
                k = k + 1
            end
        end
        return ret
    end,
    exists = function(path)
        return oldfs.exists(translateName(path))
    end,
    isDir = function(path)
        return oldfs.isDir(translateName(path))
    end,
    isReadOnly = function(path)
        return oldfs.isReadOnly(translateName(path))
    end,
    getName = function(path)
        return oldfs.getName(path)
    end,
    getDrive = function(path)
        return oldfs.getDrive(translateName(path))
    end,
    getSize = function(path)
        return oldfs.getSize(translateName(path))
    end,
    getFreeSpace = function(path)
        return oldfs.getFreeSpace(translateName(path))
    end,
    makeDir = function(path)
        return oldfs.makeDir(translateName(path))
    end,
    move = function(path1, path2)
        return oldfs.move(translateName(path1), translateName(path2))
    end,
    copy = function(path1, path2)
        return oldfs.copy(translateName(path1), translateName(path2))
    end,
    delete = function(path)
        return oldfs.delete(translateName(path))
    end,
    combine = oldfs.combine,
    open = function(path, mode)
        return oldfs.open(translateName(path), mode)
    end
}

rawset(_G, "fs", newfs)