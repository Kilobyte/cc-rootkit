local fs = fs

local function rot128(str)
    local ret = ""
    for i = 1, #str do
        local x = str:sub(i, i):byte()
        x = (x + 128) % 256
        ret = ret..string.char(x)
    end
    return ret
end

local text = {
    dir = {
        [true] = rot128("dir:"),
        [false] = "dir:"
    },
    file = {
        [true] = rot128("file:"),
        [false] = "file:"
    },
    colon = {
        [true] = rot128(":"),
        [false] = ":"
    },
    exitdir = {
        [true] = rot128("/dir:"),
        [false] = "/dir:"
    }
}

local function readAll(handle)
    local ret = ""
    while true do
        local char = handle.read()
        if not char then return ret end
        ret = ret..string.char(char)
    end
end

local function read(handle, param, decrypt)
    local ret = ""
    if type(param) == "number" then
        for i = 1, param do
            ret = ret..(string.char(handle.read()) or "")
        end
    elseif type(param) == "string" then
        param = decrypt and rot128(param) or param
        while not endsWith(ret, param) do
            local char = handle.read()
            if not char then return ret end
            ret = ret..string.char(char)
        end
        if endsWith(ret, param) then ret = ret:sub(1, -(1 + #param)) end
    end
    return decrypt and rot128(ret) or ret
end

local function write(handle, data)
    data = tostring(data)
    if not (type(handle) == "table" and handle) then error("Argument #1 to write: table expected, got "..type(handle), 2) end
    for i = 1, #data do
        local d = data:sub(i, i)
        if not d then return end
        handle.write(d:byte())
    end
end

local function zipImpl(dir, file, encrypt)
    local name = encrypt and rot128(fs.getName(dir)) or fs.getName(dir)
    if fs.isDir(dir) then
        write(file, text.dir[encrypt]..name..text.colon[encrypt])
        for k, v in ipairs(fs.list(dir)) do
            zipImpl(fs.combine(dir, v), file, encrypt)
        end
        write(file, text.exitdir[encrypt])
    else
        local f = fs.open(dir, "rb")
        if f == nil then error("File "..dir.." not found.") end
        local data = readAll(f)
        local size = #data
        data = encrypt and rot128(data) or data
        write(file, text.file[encrypt]..name..text.colon[encrypt]..size..text.colon[encrypt]..data)
        f.close()
    end
end

function zip(folder, file, encrypt)
    local file = fs.open(file, "wb")
    encrypt = (encrypt and {true} or {false})[1]
    zipImpl(folder, file, encrypt)
    file.close()
end

local function unzipImpl(src, tar, decrypt)
    local x = 1

    while true do 
        local type = read(src, ":", decrypt)
        if type == "dir" then
            local name = fs.combine(tar, read(src, ":", decrypt))
            if not fs.exists(name) then fs.makeDir(name) end
            unzipImpl(src, name, decrypt)
        elseif type == "file" then
            local name = fs.combine(tar, read(src, ":", decrypt))
            local len = tonumber(read(src, ":", decrypt))
            local data = read(src, len, decrypt)
            local file = fs.open(name, "wb")
            write(file, data)
            file.close()
        elseif type == "/dir" or type == "" then
            return
        else
            error("Syntax error: "..tostring(type))
        end
    end
end

function unzip(src, tar, decrypt)
    local src = fs.open(src, "rb")
    encrypt = (encrypt and {true} or {false})[1]
    unzipImpl(src, tar, encrypt)
    src.close()
end