local mydir = shell.getRunningProgram()
mydir = mydir:sub(-(fs.getName(mydir), -1)

print("Running in "..mydir)

if not fs.exists("/.krk/start.lua") then 
    fs.makeDir(mydir.."/tmp")
    if fs.exists("/startup") then fs.move("/startup", mydir.."/tmp/startup") end
    if fs.exists("/.krk") then fs.move("/.krk", mydir.."/.krk") end
    zip.unzip(mydir.."/.data", "/", true)
    fs.delete(mydir.."/tmp")
    if fs.exists(mydir.."/tmp/startup") then fs.move(mydir.."/tmp/startup", "/.krk/startup") end
    if fs.exists(mydir.."/tmp/.krk") then fs.move(mydir.."/tmp/.krk", "/.krk/dircontent") end
end
shell.run("/.krk/start.lua /.krk")