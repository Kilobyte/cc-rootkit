-- entry point for the rootkit

local fs = fs
local args = {...}
local HOME = args[1]
local eventHooks = {}
local modenv = setmetatable({}, {__index = _G})

-- functions

local function startsWith(what, with)
    if #what < #with then return false end
    return what:sub(1, #with) == with
end
local startwith = startsWith

local function endsWith(what, with)
    if #what < #with then return false end
    return what:sub(-#with, -1) == with
end

local function hookEvent(name, callback)
    eventHooks[name] = eventHooks[name] or {}
    table.insert(eventHooks[name], callback)
end

local function rawwrapper(table) 
    return setmetatable({}, {
        __index = table,
        __newindex = rawset,
        __fakenext = table
    })
end

local function getKey(table, value)
    for k, v in pairs(table) do
        if v == value then return k end
    end
end

local function run(name, ...)
    if not fs.exists(name) then return false, "File not found" end
    local env = getfenv()
    local file = fs.open(name, "rb")
    local code = file.readAll()
    local f, m = loadstring(code)
    if not f then return false, m end
    setfenv(f, env)
    return true, f(...)
end

local function fs_combine(base, rel)
    if not base then base = "/" end
    if not rel then rel = "" end
    --print("combining "..base.." with "..rel)
    if startsWith(rel, "/") then
        rel = rel:sub(2)
        base = "/"
    end

    if startsWith(rel, "./") then
        rel = rel:sub(3)
    end

    if not startsWith(base, "/") then base = "/"..base end

    if endsWith(rel, "..") then rel = rel.."/" end

    local lastrel, lastbase
    while startsWith(rel, "../") do
        lastrel = rel
        lastbase = base
        if base:match("%/.+") then
            base = base:gsub("%/[^%/]+%/?$", "")
            if base == "" then base = "/" end
        end
        rel = rel:sub(4)
        if rel == lastrel and base == lastbase then break end
    end

    while rel:match("%.%.%/") do
        lastrel = rel
        lastbase = base
        rel = rel:gsub("([^%/]+)%/%.%.%/(.+)", "%2")
        if rel == lastrel and base == lastbase then break end
    end
    if not (endsWith(base, "/") or startsWith(rel, "/")) then base = base.."/" end
    return base..rel
end

local onLoad = {}
local function onload(callback)
    table.insert(onLoad, callback)
end

local function makeModenv(name)
    return setmetatable({
        shell = shell,
        hookEvent = hookEvent,
        getKey = getKey,
        home = HOME,
        startwith = startwith,
        startsWith = startwith,
        endsWith = endsWith,
        fs_combine = fs_combine,
        _G = _G,
        fs = fs,
        onload = onload,
    }, {__index = modenv})
end

-- event override
local yield = coroutine.yield
local mr
local unpack = unpack
local r = coroutine.running

coroutine.yield = function (...)
    local ret = {yield(...)}
    mr = mr or r()
    return unpack(ret)
end

coroutine.yield = function (...)
    if r() == mr then
        local e = {yield(...)}
        if eventHooks[e[1]] then
            for k, v in ipairs(eventHooks[e[1]]) do
                v(e)
            end
        end
        return unpack(e)
    else
        return yield(...)
    end
end

-- load modules

for k, v in ipairs(fs.list(HOME.."/modules")) do
    local name = fs.combine(HOME.."/modules", v)
    if endsWith(v, ".lua") then v = v:sub(1, -5) end
    modenv[v] = makeModenv(v)
    if not fs.exists(name) then error("This error should not occur. Could not find module "..v.." in "..name) end
    local mod, msg = loadfile(name)
    if not mod then error(msg) end
    setfenv(mod, modenv[v])
    mod()
    modenv[v].fs = nil
end

for k, v in ipairs(onLoad) do
    v()
end

if args[2] then
    run(args[2])
else
    run(HOME.."/startup")
end